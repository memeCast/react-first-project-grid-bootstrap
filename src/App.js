import React, { Component } from 'react';
import './App.css';
import Lista from './componentes/lista.jsx'

class App extends Component {
	constructor(){
		super();
		this.state = {nuevoItem: "",arregloDeItems:[]},
		this.manejarCambio=this.manejarCambio.bind(this),
		this.agregarALista = this.agregarALista.bind(this)
	}
	agregarALista(){
		this.state.arregloDeItems.push(this.state.nuevoItem);
		this.setState({nuevoItem:``});
		// this.setState(nuevoItemTexto:"");
	}
	manejarCambio(event){
		let inputValue =event.target.value;
		this.setState({nuevoItem:inputValue});

		// event.target.value
	}
  render() {
    return (
      <div className="App">
      	<input type="text" onChange={this.manejarCambio} value={this.state.nuevoItem}/>
      	<button onClick={this.agregarALista}>Enviar</button>
      	<Lista tituloLista="Cositas Kawaii" elementosLista={this.state.arregloDeItems} />

      </div>
    );
  }
}

export default App;
